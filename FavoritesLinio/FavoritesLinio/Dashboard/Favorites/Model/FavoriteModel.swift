//
//  FavoriteModel.swift
//  FavoritesLinio
//
//  Created by roreyesl on 22/10/21.
//

import Foundation

public struct detailFavorite : Codable{
    public var dId              : Double?
    public var strName          : String?
    public var strDescription   : String?
    public var bDefault         : Bool?
    public var arrOwner         : arrOwnerResponse?
    public var strCreated       : String?
    public var strVisibility    : String?
    public var arrProducts      : [String:productsDetailResponse]?
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        dId     =  try (container.decodeIfPresent(Double.self, forKey: .dId))
        strName    =  try (container.decodeIfPresent(String.self, forKey: .strName))
        strDescription    =  try (container.decodeIfPresent(String.self, forKey: .strDescription))
        bDefault    =  try (container.decodeIfPresent(Bool.self, forKey: .bDefault))
        arrOwner    =  try (container.decodeIfPresent(arrOwnerResponse.self, forKey: .arrOwner))
        strCreated    =  try (container.decodeIfPresent(String.self, forKey: .strCreated))
        strVisibility    =  try (container.decodeIfPresent(String.self, forKey: .strVisibility))
        arrProducts    =  try (container.decodeIfPresent([String:productsDetailResponse].self, forKey: .arrProducts))
    }
    
    private enum CodingKeys : String, CodingKey{
        case dId              = "id"
        case strName          = "name"
        case strDescription   = "description"
        case bDefault         = "default"
        case arrOwner         = "owner"
        case strCreated       = "createdAt"
        case strVisibility    = "visibility"
        case arrProducts      = "products"
    }
    
    public init(){}
}

public struct arrOwnerResponse : Codable{
   
    public var strName          : String?
    public var strEmail         : String?
    public var strLinioId       : String?
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        strName    =  try (container.decodeIfPresent(String.self, forKey: .strName))
        strEmail    =  try (container.decodeIfPresent(String.self, forKey: .strEmail))
        strLinioId    =  try (container.decodeIfPresent(String.self, forKey: .strLinioId))
    }
    
    private enum CodingKeys : String, CodingKey{
        case strName        = "name"
       case strEmail        = "email"
        case strLinioId     = "linioId"
    }
    
    public init(){}
}

public struct productsDetailResponse : Codable{
  
    public var id                : Double?
    public var strName           : String?
    public var iWishListPrice    : Int?
    public var strSlug           : String?
    public var strUrl            : String?
    public var strImage          : String?
    public var iLinioPlusLevel   : Int?
    public var strConditionType  : String?
    public var bFreeShipping     : Bool?
    public var bImported         : Bool?
    public var bActive           : Bool?
    
   
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id =  try (container.decodeIfPresent(Double.self, forKey: .id))
        strName =  try (container.decodeIfPresent(String.self, forKey: .strName))
        iWishListPrice =  try (container.decodeIfPresent(Int.self, forKey: .iWishListPrice))
        strSlug =  try (container.decodeIfPresent(String.self, forKey: .strSlug))
        strUrl =  try (container.decodeIfPresent(String.self, forKey: .strUrl))
        strImage =  try (container.decodeIfPresent(String.self, forKey: .strImage))
        iLinioPlusLevel =  try (container.decodeIfPresent(Int.self, forKey: .iLinioPlusLevel))
        strConditionType =  try (container.decodeIfPresent(String.self, forKey: .strConditionType))
        bFreeShipping =  try (container.decodeIfPresent(Bool.self, forKey: .bFreeShipping))
        bImported =  try (container.decodeIfPresent(Bool.self, forKey: .bImported))
        bActive =  try (container.decodeIfPresent(Bool.self, forKey: .bActive))
    }
    
    private enum CodingKeys : String, CodingKey{
        case id                = "id"
        case strName           = "name"
        case iWishListPrice    = "wishListPrice"
        case strSlug           = "slug"
        case strUrl            = "url"
        case strImage          = "image"
        case iLinioPlusLevel   = "linioPlusLevel"
        case strConditionType  = "conditionType"
        case bFreeShipping     = "freeShipping"
        case bImported         = "imported"
        case bActive           = "active"
    }
    
    public init(){}
}
          
