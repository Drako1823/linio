//
//  AlertGeneric.swift
//  BookStoreKAVAK
//
//  Created by roreyesl on 13/09/21.
//

import Foundation
import UIKit

class AlertGeneric {
    
    //MARK: - Functions
    
    static public func simpleWith(title        : String? = "Linio",
                                  message      : String?,
                                  actionTitle  : String = "Aceptar",
                                  actionHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let alertController = UIAlertController(title         : title,
                                                message       : message ?? "",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title  : actionTitle,
                                                style  : .default,
                                                handler: actionHandler))
        return alertController
    }
}
