//
//  IntExtension.swift
//  BookStoreKAVAK
//
//  Created by roreyesl on 13/09/21.
//

import Foundation

extension Int {
    
    //MARK: - Properties
    public var isSuccess: Bool { return self == 200 }
}
