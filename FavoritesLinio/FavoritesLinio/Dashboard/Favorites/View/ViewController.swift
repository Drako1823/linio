//
//  ViewController.swift
//  FavoritesLinio
//
//  Created by roreyesl on 22/10/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var cllFavorites: UICollectionView!{
        didSet{
            cllFavorites.register(UINib(nibName: "AllProductsInSectionCollectionViewCell", bundle: Bundle(for: AllProductsInSectionCollectionViewCell.self)), forCellWithReuseIdentifier: "allProductsInSectionCollectionViewCell")
            cllFavorites.register(UINib(nibName: "DetailProductsCollectionViewCell", bundle: Bundle(for: DetailProductsCollectionViewCell.self)), forCellWithReuseIdentifier: "detailProductsCollectionViewCell")
            cllFavorites.register(TitleHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
        }
    }
    lazy var vmFavorite = FavoriteViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vmFavorite.loadInfoFavorites { error in
            if error.code.isSuccess {
                self.cllFavorites.reloadData()
            }else{
                self.present(AlertGeneric.simpleWith(message: "Se genero un error: \(error.code.description) "), animated: true, completion: nil)
            }
        }
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            return CGSize(width:self.view.frame.size.width / 2.1, height: 195)
        case 1:
            return CGSize(width:self.view.frame.size.width / 2.1, height: 180)
        default:
            return CGSize(width:self.view.frame.size.width / 2.1, height: 180)
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "allProductsInSectionCollectionViewCell", for: indexPath) as? AllProductsInSectionCollectionViewCell else { return UICollectionViewCell() }
            cell.instantiateCell(withRow: indexPath.row, withSectionSelected: vmFavorite.getAllDataInSection(withSection: indexPath.row))
            return cell
        case 1:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "detailProductsCollectionViewCell", for: indexPath) as? DetailProductsCollectionViewCell else { return UICollectionViewCell() }
            cell.initDetailSection(withProductSelected: vmFavorite.getDataInSection(withProductSelected: indexPath.row))
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return vmFavorite.getNumberOfRows()
        case 1:
            return vmFavorite.getNumberOfRowsAllProducts()
        default:
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if indexPath.section == 1{
            if kind == UICollectionView.elementKindSectionHeader {
                 let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! TitleHeader
                 sectionHeader.label.text = "Todos mis favoritos( \(vmFavorite.getNumberOfRowsAllProducts()))"
                 return sectionHeader
            } else {
                 return UICollectionReusableView()
            }
        }else {
            return UICollectionReusableView()
       }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 1{
            return CGSize(width: collectionView.frame.width, height: 40)
        }else{
            return CGSize(width: collectionView.frame.width, height: 0)
        }
    }
}
