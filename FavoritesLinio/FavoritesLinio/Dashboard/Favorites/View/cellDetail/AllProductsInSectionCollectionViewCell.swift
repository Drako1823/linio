//
//  AllProductsInSectionCollectionViewCell.swift
//  FavoritesLinio
//
//  Created by roreyesl on 22/10/21.
//

import UIKit

class AllProductsInSectionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var vwFirst: UIView!
    @IBOutlet weak var vwSecond: UIView!
    @IBOutlet weak var vwThird: UIView!
    @IBOutlet weak var imgFirst: UIImageView!
    @IBOutlet weak var imgSecond: UIImageView!
    @IBOutlet weak var imgThird: UIImageView!
    @IBOutlet weak var lblTitleSection: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    
    override func awakeFromNib() { super.awakeFromNib() }
    
    public func instantiateCell(withRow iRow:Int, withSectionSelected arrSection:detailFavorite?){
        guard let section = arrSection else{return}
        
        self.imgFirst.image = UIImage(named: "imgPlacerHolderImage")?.resized(to: CGSize(width: self.imgFirst.frame.size.width, height: self.imgFirst.frame.size.height))
        self.imgSecond.image = UIImage(named: "imgPlacerHolderImage")?.resized(to: CGSize(width: self.imgSecond.frame.size.width, height: self.imgSecond.frame.size.height))
        self.imgThird.image = UIImage(named: "imgPlacerHolderImage")?.resized(to: CGSize(width: self.imgThird.frame.size.width, height: self.imgThird.frame.size.height))

        vwFirst.layer.cornerRadius = 5
        vwSecond.layer.cornerRadius = 5
        vwThird.layer.cornerRadius = 5

        if let product = section.arrProducts{
            for (key,value) in product.enumerated() {
                if key == 0 {
                    if let url = URL(string: value.value.strImage ?? ""){
                        UIImage.cacheImage(from: url, name: url.description){ image in
                            DispatchQueue.main.async {
                                self.imgFirst.image = image?.resized(to: CGSize(width: self.vwFirst.frame.size.width, height: self.vwFirst.frame.size.height))
                            }
                        }
                    }
                }else  if key == 1 {
                    if let url = URL(string: value.value.strImage ?? ""){
                        UIImage.cacheImage(from: url, name: url.description){ image in
                            DispatchQueue.main.async {
                                self.imgSecond.image = image?.resized(to: CGSize(width: self.vwSecond.frame.size.width, height: self.vwSecond.frame.size.height))
                            }
                        }
                    }
                }else  if key == 2 {
                    if let url = URL(string: value.value.strImage ?? ""){
                        UIImage.cacheImage(from: url, name: url.description){ image in
                            DispatchQueue.main.async {
                                self.imgThird.image = image?.resized(to: CGSize(width: self.vwThird.frame.size.width, height: self.vwThird.frame.size.height))
                            }
                        }
                    }
                }
            }
        }
        
        lblTitleSection.text = "\(section.strName ?? "")"
        lblNumber.text = "\(section.arrProducts?.count ?? 0)"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgFirst.image = nil
        imgSecond.image = nil
        imgThird.image = nil
    }
    
}
