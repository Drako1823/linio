//
//  DetailProductsCollectionViewCell.swift
//  FavoritesLinio
//
//  Created by roreyesl on 22/10/21.
//

import UIKit

class DetailProductsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var vwAllComponents: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var imgLevel1: UIImageView!
    @IBOutlet weak var imgLevel2: UIImageView!
    @IBOutlet weak var imgRefurbished: UIImageView!
    @IBOutlet weak var imgNew: UIImageView!
    @IBOutlet weak var imgInternational: UIImageView!
    @IBOutlet weak var imgFreeShipping: UIImageView!

    override func awakeFromNib() { super.awakeFromNib() }
    
    public func initDetailSection(withProductSelected arrProduct:productsDetailResponse?){
        guard let arrProduct = arrProduct else{return}
        self.vwAllComponents.layer.cornerRadius = 10

        self.imgProduct.image = UIImage(named: "imgPlacerHolderImage")?.resized(to: CGSize(width: self.contentView.frame.size.width, height: 180))

        if let url = URL(string: arrProduct.strImage ?? ""){
            UIImage.cacheImage(from: url, name: url.description){ image in
                DispatchQueue.main.async {
                    self.imgProduct.image = image?.resized(to: CGSize(width: self.contentView.frame.size.width, height: 180))
                }
            }
        }
        
        let stcBadge = UIStackView(frame: CGRect(x: 4, y: 4, width: 30, height: 30))
        if arrProduct.iLinioPlusLevel == 1 {
            stcBadge.addArrangedSubview(imgLevel1)
        }
        if arrProduct.iLinioPlusLevel == 2 {
            stcBadge.addArrangedSubview(imgLevel2)
        }
        if arrProduct.strConditionType == "refurbished" {
            stcBadge.addArrangedSubview(imgRefurbished)
        }
        if arrProduct.strConditionType == "new" {
            stcBadge.addArrangedSubview(imgNew)
        }
        if arrProduct.bImported ?? false {
            stcBadge.addArrangedSubview(imgInternational)
        }
        if arrProduct.bFreeShipping ?? false {
            stcBadge.addArrangedSubview(imgFreeShipping)
        }
        
        stcBadge.axis  = NSLayoutConstraint.Axis.vertical
        stcBadge.distribution  = UIStackView.Distribution.equalSpacing
        stcBadge.alignment = UIStackView.Alignment.center
        stcBadge.spacing   = 2
        stcBadge.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(stcBadge)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgProduct.image = nil
    }
    
}
