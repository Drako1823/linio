//
//  FavoriteViewModel.swift
//  FavoritesLinio
//
//  Created by roreyesl on 22/10/21.
//

import Foundation

public class FavoriteViewModel : Decodable{
    public typealias CompletionBlock = (NSError) -> Void
    private var serviceManager : Webservice?
    private let URL_GET_FAVORITE_DATA = "aletomm90/7ff8e9a7c49aefd06a154fe097028d27/raw/c87e2e7d21313391d412420b4254c391aa68eeec/favorites.json"
    public init(_ serviceManager : Webservice = Webservice()){
        self.serviceManager = serviceManager
    }
    public var detailFavorites: [detailFavorite]?
    public var allDetailFavorites = [productsDetailResponse]()

    // MARK: - LifeCycle
    required public init(from decoder: Decoder) throws {}
    
    // MARK: - Functions
    
    public func loadInfoFavorites(withCompletionHandler handler: @escaping CompletionBlock){
        
        var resource = Resource<[detailFavorite]>(URL_GET_FAVORITE_DATA, .baseCore)
        resource.httpMethod = .get
        resource.showProgress = true
        serviceManager?.load(resource: resource){ result in
            switch result{
            case .success(let detailFavorites, let error):
                self.detailFavorites = detailFavorites
                handler(error ?? NSError(domain: "Linio", code: error?.code ?? 500, userInfo: error?.userInfo))
            case .failure(let error):
                handler(error ?? NSError(domain:"Linio", code: error?.code ?? 500, userInfo: error?.userInfo))
            }
        }
    }
    
    public func getNumberOfRows() -> Int {
        return detailFavorites?.count ?? 0
    }
    
    public func getNumberOfRowsAllProducts() -> Int {
        var iSum = 0
        detailFavorites?.forEach({ n in
            n.arrProducts?.forEach({ (key: String, value: productsDetailResponse) in
                iSum += 1
                allDetailFavorites.append(value)
            })
        })
        return iSum
    }
    
    public func getAllDataInSection(withSection iSection:Int) -> detailFavorite? {
        return self.detailFavorites?[iSection]
    }
    
    public func getDataInSection(withProductSelected iSelected:Int) -> productsDetailResponse {
            return self.allDetailFavorites[iSelected]
    }
}
